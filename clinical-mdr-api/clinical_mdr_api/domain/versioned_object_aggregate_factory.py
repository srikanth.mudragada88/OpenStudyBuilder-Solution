class VersionedObjectFactory:
    """
    A VersionedObjectFactory is responsible for creating VersionedObjectAR objects.
    This class is abstract, and should be extended for a domain entity.
    """
