from clinical_mdr_api.models.activities.categoric_finding import (
    CategoricFinding,
    CategoricFindingCreateInput,
    CategoricFindingEditInput,
    CategoricFindingVersion,
)


class LaboratoryActivity(CategoricFinding):
    pass


class LaboratoryActivityCreateInput(CategoricFindingCreateInput):
    pass


class LaboratoryActivityEditInput(CategoricFindingEditInput):
    pass


class LaboratoryActivityVersion(CategoricFindingVersion):
    pass
