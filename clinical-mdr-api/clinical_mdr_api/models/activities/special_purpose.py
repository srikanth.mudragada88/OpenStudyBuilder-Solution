from clinical_mdr_api.models.activities.activity_instance import (
    ActivityInstance,
    ActivityInstanceCreateInput,
    ActivityInstanceEditInput,
    ActivityInstanceVersion,
)


class SpecialPurpose(ActivityInstance):
    pass


class SpecialPurposeCreateInput(ActivityInstanceCreateInput):
    pass


class SpecialPurposeEditInput(ActivityInstanceEditInput):
    pass


class SpecialPurposeVersion(ActivityInstanceVersion):
    pass
