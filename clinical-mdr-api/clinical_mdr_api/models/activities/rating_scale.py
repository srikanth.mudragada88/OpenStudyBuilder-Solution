from clinical_mdr_api.models.activities.categoric_finding import (
    CategoricFinding,
    CategoricFindingCreateInput,
    CategoricFindingEditInput,
    CategoricFindingVersion,
)


class RatingScale(CategoricFinding):
    pass


class RatingScaleCreateInput(CategoricFindingCreateInput):
    pass


class RatingScaleEditInput(CategoricFindingEditInput):
    pass


class RatingScaleVersion(CategoricFindingVersion):
    pass
