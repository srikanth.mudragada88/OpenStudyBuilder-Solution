import api from './templates'

const basePath = '/activity-description-templates'

const activityDescriptionTemplates = api(basePath)

export default activityDescriptionTemplates
