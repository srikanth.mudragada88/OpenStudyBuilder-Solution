const LIBRARY_SPONSOR = 'Sponsor'
const LIBRARY_USER_DEFINED = 'User Defined'

export default {
  LIBRARY_SPONSOR,
  LIBRARY_USER_DEFINED
}
