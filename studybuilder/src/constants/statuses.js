const DRAFT = 'Draft'
const FINAL = 'Final'
const RETIRED = 'Retired'

export default {
  DRAFT,
  FINAL,
  RETIRED
}
