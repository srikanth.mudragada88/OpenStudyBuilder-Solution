const ITEM = 'item'
const GROUP = 'group'
const FORM = 'form'
const TEMPLATE = 'template'

export default {
  ITEM,
  GROUP,
  FORM,
  TEMPLATE
}
