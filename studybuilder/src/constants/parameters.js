
const NUM_VALUE = 'NumericValue'
const TEXT_VALUE = 'TextValue'
const NULL = 'null'

export default {
  NUM_VALUE,
  TEXT_VALUE,
  NULL
}
