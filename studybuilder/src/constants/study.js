const DESCRIPTION_METADATA = '+currentMetadata.studyDescription'
const HIGH_LEVEL_STUDY_DESIGN_METADATA = '+currentMetadata.highLevelStudyDesign'
const IDENTIFICATION_METADATA = '+currentMetadata.identificationMetadata'
const INTERVENTION_METADATA = '+currentMetadata.studyIntervention'
const POPULATION_METADATA = '+currentMetadata.studyPopulation'
const VERSION_METADATA = '+currentMetadata.versionMetadata'

const STOP_RULE_NONE = 'NONE'

const TERM_NOT_APPLICABLE = 'C48660_NA'

export default {
  DESCRIPTION_METADATA,
  HIGH_LEVEL_STUDY_DESIGN_METADATA,
  IDENTIFICATION_METADATA,
  INTERVENTION_METADATA,
  POPULATION_METADATA,
  VERSION_METADATA,
  STOP_RULE_NONE,
  TERM_NOT_APPLICABLE
}
